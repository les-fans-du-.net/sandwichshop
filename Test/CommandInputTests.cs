﻿using FluentAssertions;
using Models;
using SandwichShop.CommandSystem;
using SandwichShop.Factory;
using System;
using System.IO;
using Xunit;

namespace Test;

public class CommandInputTests
{
    private readonly SandwichFactory sandwichFactory = new JsonSandwichFactory(Directory.GetParent(Directory.GetCurrentDirectory()).Parent.Parent.Parent + @"/Menu.json");
    private readonly CommandParser commandParser;

    public CommandInputTests()
    {
        this.commandParser = new CommandParser(',', '+', this.sandwichFactory);
    }

    [Fact(DisplayName = "One order with one client")]
    public void CommandInput_1()
    {
        Command expectedCommand = new Command();
        expectedCommand.Add(new Order("person_A", this.sandwichFactory.CreateSandwich("ham")));

        Command command = this.commandParser.Parse("person_A ham");

        command.GetOrders().Should().BeEquivalentTo(expectedCommand.GetOrders(), options =>
        {
            options.AllowingInfiniteRecursion();
            return options;
        });
    }

    [Fact(DisplayName = "Two orders with one different client each")]
    public void CommandInput_2()
    {
        Command expectedCommand = new Command();
        expectedCommand.Add(new Order("person_A", this.sandwichFactory.CreateSandwich("ham")));
        expectedCommand.Add(new Order("person_B", this.sandwichFactory.CreateSandwich("chicken")));

        Command command = this.commandParser.Parse("person_A ham, person_B chicken");

        command.GetOrders().Should().BeEquivalentTo(expectedCommand.GetOrders(), options =>
        {
            options.AllowingInfiniteRecursion();
            return options;
        });
    }

    [Fact(DisplayName = "One order with two different clients")]
    public void CommandInput_3()
    {
        Command expectedCommand = new Command();
        expectedCommand.Add(new Order("person_A", this.sandwichFactory.CreateSandwich("ham")));
        expectedCommand.Add(new Order("person_B", this.sandwichFactory.CreateSandwich("ham")));

        Command command = this.commandParser.Parse("person_A + person_B ham");

        command.GetOrders().Should().BeEquivalentTo(expectedCommand.GetOrders(), options =>
        {
            options.AllowingInfiniteRecursion();
            return options;
        });
    }

    [Fact(DisplayName = "One order with three different clients")]
    public void CommandInput_4()
    {
        Command expectedCommand = new Command();
        expectedCommand.Add(new Order("person_A", this.sandwichFactory.CreateSandwich("ham")));
        expectedCommand.Add(new Order("person_B", this.sandwichFactory.CreateSandwich("ham")));
        expectedCommand.Add(new Order("person_C", this.sandwichFactory.CreateSandwich("ham")));

        Command command = this.commandParser.Parse("person_A + person_B + person_C ham");

        command.GetOrders().Should().BeEquivalentTo(expectedCommand.GetOrders(), options =>
        {
            options.AllowingInfiniteRecursion();
            return options;
        });
    }

    [Fact(DisplayName = "Two orders with one and two clients, different client every time")]
    public void CommandInput_5()
    {
        Command expectedCommand = new Command();
        expectedCommand.Add(new Order("person_A", this.sandwichFactory.CreateSandwich("ham")));
        expectedCommand.Add(new Order("person_B", this.sandwichFactory.CreateSandwich("chicken")));
        expectedCommand.Add(new Order("person_C", this.sandwichFactory.CreateSandwich("chicken")));

        Command command = this.commandParser.Parse("person_A ham, person_B + person_C chicken");

        command.GetOrders().Should().BeEquivalentTo(expectedCommand.GetOrders(), options =>
        {
            options.AllowingInfiniteRecursion();
            return options;
        });
    }

    [Fact(DisplayName = "Two orders with one and two clients, same client every time")]
    public void CommandInput_6()
    {
        Command expectedCommand = new Command();
        expectedCommand.Add(new Order("person_A", this.sandwichFactory.CreateSandwich("ham")));
        expectedCommand.Add(new Order("person_A", this.sandwichFactory.CreateSandwich("ham")));
        expectedCommand.Add(new Order("person_A", this.sandwichFactory.CreateSandwich("ham")));

        Command command = this.commandParser.Parse("person_A + person_A ham, person_A ham");

        command.GetOrders().Should().BeEquivalentTo(expectedCommand.GetOrders(), options =>
        {
            options.AllowingInfiniteRecursion();
            return options;
        });
    }

    [Fact(DisplayName = "Sandwich name case should be ignored")]
    public void CommandInput_7()
    {
        Command expectedCommand = new Command();
        expectedCommand.Add(new Order("person_A", this.sandwichFactory.CreateSandwich("chicken")));

        Command command = this.commandParser.Parse("person_A ChIcKeN");

        command.GetOrders().Should().BeEquivalentTo(expectedCommand.GetOrders(), options =>
        {
            options.AllowingInfiniteRecursion();
            return options;
        });
    }

    [Fact(DisplayName = "Adding a sandwich by any of its accepted name should work")]
    public void CommandInput_9()
    {
        Command expectedCommand = new Command();
        expectedCommand.Add(new Order("person_A", this.sandwichFactory.CreateSandwich("ham")));
        expectedCommand.Add(new Order("person_A", this.sandwichFactory.CreateSandwich("ham")));
        expectedCommand.Add(new Order("person_A", this.sandwichFactory.CreateSandwich("ham")));

        Command command = this.commandParser.Parse("person_A ham, person_A ham sandwich, person_A hamsandwich");

        command.GetOrders().Should().BeEquivalentTo(expectedCommand.GetOrders(), options =>
        {
            options.AllowingInfiniteRecursion();
            return options;
        });
    }

    [Fact(DisplayName = "A command with spaces between every client, sandwich and separators should be formatted")]
    public void CommandInput_10()
    {
        Command expectedCommand = new Command();
        expectedCommand.Add(new Order("person_A", this.sandwichFactory.CreateSandwich("ham")));
        expectedCommand.Add(new Order("person_A", this.sandwichFactory.CreateSandwich("chicken")));
        expectedCommand.Add(new Order("person_B", this.sandwichFactory.CreateSandwich("chicken")));
        expectedCommand.Add(new Order("person_C", this.sandwichFactory.CreateSandwich("dieppois")));
        expectedCommand.Add(new Order("person_A", this.sandwichFactory.CreateSandwich("dieppois")));
        expectedCommand.Add(new Order("person_B", this.sandwichFactory.CreateSandwich("dieppois")));
        expectedCommand.Add(new Order("person_A", this.sandwichFactory.CreateSandwich("ham")));
        expectedCommand.Add(new Order("person_C", this.sandwichFactory.CreateSandwich("ham")));

        Command command = this.commandParser.Parse(
            " person_A    ham     sandwich    ," +
            "   person_A    +    person_B    chicken   ," +
            "  person_C + person_A+    person_B    dieppois   ," +
            "person_A+person_C ham");

        command.GetOrders().Should().BeEquivalentTo(expectedCommand.GetOrders(), options =>
        {
            options.AllowingInfiniteRecursion();
            return options;
        });
    }

    [Fact(DisplayName = "A command with an unknown sandwich should throw an exception")]
    public void CommandInput_11()
    {
        Action action = () => this.commandParser.Parse("person_A bubbletea");

        action.Should()
            .Throw<SandwichNotFoundException>()
            .WithMessage("Error: sandwich 'bubbletea' does not exist.");
    }
}
