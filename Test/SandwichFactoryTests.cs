﻿using FluentAssertions;
using Models;
using SandwichShop.Factory;
using System;
using System.IO;
using Xunit;

namespace Test;

public class SandwichFactoryTests
{
    private readonly SandwichFactory sandwichFactory = new JsonSandwichFactory(Directory.GetParent(Directory.GetCurrentDirectory()).Parent.Parent.Parent + @"/Menu.json");

    [Fact(DisplayName =
        "Getting a sandwich by name 'ham'\n" +
        "Should return a Ham sandwich")]
    public void SandwichFactory_1()
    {
        Sandwich sandwich = this.sandwichFactory.CreateSandwich("ham");
        sandwich.DisplayName.Should().Be("Ham sandwich");
    }

    [Fact(DisplayName =
        "Getting a sandwich by name 'hm'\n" +
        "Should return a Ham sandwich\n" +
        "Because similarity score is high enough")]
    public void SandwichFactory_2()
    {
        Sandwich sandwich = this.sandwichFactory.CreateSandwich("hm");
        sandwich.DisplayName.Should().Be("Ham sandwich");
    }

    [Fact(DisplayName =
        "Getting a sandwich by name 'bonjour'\n" +
        "Should not return any sandwich\n" +
        "Because similarity score is too low")]
    public void SandwichFactory_3()
    {
        Action getSandwich = () => this.sandwichFactory.CreateSandwich("bonjour");

        getSandwich.Should()
            .Throw<SandwichNotFoundException>()
            .WithMessage("Error: sandwich 'bonjour' does not exist.");
    }
}
