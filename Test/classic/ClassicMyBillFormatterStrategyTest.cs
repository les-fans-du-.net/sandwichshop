using System;
using System.Collections.Generic;
using System.IO;
using FluentAssertions;
using Models;
using SandwichShop.Factory;
using SandwichShop.Utilitaries.Bill;
using Xunit;

namespace Test.classic;

public class ClassicMyBillFormatterStrategyTest
{
    private SandwichFactory sandwichFactory;

    public ClassicMyBillFormatterStrategyTest()
    {
        this.sandwichFactory = new JsonSandwichFactory(Directory.GetParent(Directory.GetCurrentDirectory()).Parent.Parent.Parent + @"/Menu.json");
    }


    [Fact(DisplayName = "format an empty command")]
    public void should_get_an_empty_string_with_an_empty_command()
    {
        MyBillFormatterStrategy myBillFormatterStrategy = new ClassicMyBillFormatterStrategy();
        Command command = new Command();


        string result = myBillFormatterStrategy.format(command);
        result.Should().Be("Prix Total: 0e");
    }
    
    [Fact(DisplayName = "format a command with one sandwich")]
    public void should_get_an_command_with_one_sandwich()
    {
        MyBillFormatterStrategy myBillFormatterStrategy = new ClassicMyBillFormatterStrategy();
        Command command = new Command();
        string firstIndex = "A";
        Sandwich sandwich1 = this.sandwichFactory.CreateSandwich("ham");
        string Ingredients1 = this.getDisplayIngredients(sandwich1);
        
        command.Add(new Order(firstIndex, sandwich1));
        string result = myBillFormatterStrategy.format(command);

        result.Should().Be($"{firstIndex} {sandwich1.DisplayName}\n" +
                           $"{Ingredients1}" +
                           $"Prix Total: {sandwich1.Price}e");
    }
    
    [Fact(DisplayName = "format a command with two sandwich")]
    public void should_get_an_command_with_two_sandwich()
    {
        MyBillFormatterStrategy myBillFormatterStrategy = new ClassicMyBillFormatterStrategy();
        Command command = new Command();
        string firstIndex = "A";
        string secundIndex = "B";
        Sandwich sandwich1 = this.sandwichFactory.CreateSandwich("ham");
        string Ingredients1 = this.getDisplayIngredients(sandwich1);
        Sandwich sandwich2 = this.sandwichFactory.CreateSandwich("chicken");
        string Ingredients2 = this.getDisplayIngredients(sandwich2);
        
        command.Add(new Order(firstIndex, sandwich1));
        command.Add(new Order(secundIndex, sandwich2));
        string result = myBillFormatterStrategy.format(command);

        result.Should().Be($"{firstIndex} {sandwich1.DisplayName}\n" +
                           $"{Ingredients1}" +
                           $"{secundIndex} {sandwich2.DisplayName}\n" +
                           $"{Ingredients2}" +
                           $"Prix Total: {sandwich1.Price + sandwich2.Price}e");
    }


    private string getDisplayIngredients(Sandwich sandwich)
    {
        string ingredients = "";
        foreach (Ingredient ingredient in sandwich.Ingredients)
        {
            ingredients += $"\t{ingredient.Name}: {ingredient.Quantity}{ingredient.Unit}\n";
        }

        return ingredients;
    }
}