using System;
using System.IO;
using System.Linq;
using FluentAssertions;
using Models;
using SandwichShop.CommandSystem;
using SandwichShop.Factory;
using Xunit;

namespace Test.classic;

public class ParserTest
{
    private SandwichFactory SandwichFactory = SandwichFactory.Create(SandwichInputFormat.JSON,Directory.GetParent(Directory.GetCurrentDirectory()).Parent.Parent.Parent + @"/Menu.json");
    private ParserConfiguration ParserConfiguration = new ParserConfigurationBuilder()
        .WithSandwichSeparator(',')
        .WithIndexSandwichSeparator(' ')
        .WithIndexSandwichChars("ABCDEFGHIJKLMNOPQRSTUVWXYZ")
        .Build();

    private Parser parser;

    public ParserTest()
    {
        this.parser = new Parser(ParserConfiguration, SandwichFactory);
    }
    
    [Fact(DisplayName = "With an empty command")]
    public void should_get_an_empty_my_command_without_errors()
    {
        Action action = () => this.parser.Parse("");
        
        action.Should()
            .Throw<ArgumentException>();
    }

    
    [Fact(DisplayName = "parse one input Sandwich")]
    public void should_parse_one_sandwich()
    {
        Command myCommand;
        
        
        myCommand = this.parser.Parse("A Dieppois");

        
        myCommand.GetOrders().ToList().Count.Should().Be(1);
        Order classicOrder = myCommand.GetOrders().ToList()[0];
        classicOrder.Index.Should().Be("A");
        classicOrder.Sandwich.AcceptedNames.Should().Contain("Dieppois");

    }
    
    [Fact(DisplayName = "parse two input Sandwich")]
    public void should_parse_two_sandwiches()
    {
        Command myCommand;
        
        
        myCommand = this.parser.Parse("A Dieppois, B Chicken");

        
        myCommand.GetOrders().ToList().Count.Should().Be(2);
        Order classicOrder1 = myCommand.GetOrders().ToList()[0];
        Order classicOrder2 = myCommand.GetOrders().ToList()[1];
        classicOrder1.Index.Should().Be("A");
        classicOrder1.Sandwich.AcceptedNames.Should().Contain("Dieppois");
        
        classicOrder2.Index.Should().Be("B");
        classicOrder2.Sandwich.AcceptedNames.Should().Contain("Chicken");

    }
    
    [Fact(DisplayName = "parse two input Sandwiches but don't find separator")]
    public void should_parse_two_sandwiches_but_throw_in_error_for_bad_format()
    {
        Action action = () => this.parser.Parse("A Dieppois B Chicken");

        action.Should()
            .Throw<SandwichShop.Factory.SandwichNotFoundException>();
    }
    
    [Fact(DisplayName = "Adding a sandwich by any of its accepted name should work")]
    public void should_parse_3_different_sandwiches()
    {
        Command myCommandResult = new Command();
        myCommandResult.Add(new Order("A", this.SandwichFactory.CreateSandwich("ham")));
        myCommandResult.Add(new Order("B", this.SandwichFactory.CreateSandwich("ham")));
        myCommandResult.Add(new Order("C", this.SandwichFactory.CreateSandwich("ham")));

        Command myCommand = this.parser.Parse("A ham, B ham sandwich, C hamsandwich");

        myCommand.GetOrders().Should().BeEquivalentTo(myCommandResult.GetOrders(), options =>
        {
            options.AllowingInfiniteRecursion();
            return options;
        });
    }
    
    [Fact(DisplayName = "Adding an empty second sandwich")]
    public void should_throw_error_because_first_sandwich_is_valid_but_second_is_empty()
    {
        Action action = () => this.parser.Parse("A Dieppois,");

        action.Should()
            .Throw<ArgumentException>();
    }
    
    [Fact(DisplayName = "the command have only an index")]
    public void should_throw_error_because_sandwich_command_not_contain_sandwich()
    {
        Action action = () => this.parser.Parse("A ");

        action.Should()
            .Throw<ArgumentException>();
    }
    
    [Fact(DisplayName = "the command have only an index without spaces")]
    public void should_throw_error_because_sandwich_command_not_contain_sandwich_and_spaces()
    {
        Action action = () => this.parser.Parse("A");

        action.Should()
            .Throw<ArgumentException>();
    }
    
    [Fact(DisplayName = "the command have not a good index for a sandwich")]
    public void should_throw_error_because_index_is_not_valid()
    {
        Action action = () => this.parser.Parse("C Dieppois");

        action.Should()
            .Throw<IndexNotValidException>()
            .WithMessage("Error: index 'C' is not valid.");
    }
    
    [Fact(DisplayName = "A command with spaces between every client, sandwich and separators should be formatted")]
    public void should_parse_a_command_with_many_spaces()
    {
        Command expectedCommand = new Command();
        expectedCommand.Add(new Order("A", this.SandwichFactory.CreateSandwich("ham")));
        expectedCommand.Add(new Order("B", this.SandwichFactory.CreateSandwich("chicken")));
        expectedCommand.Add(new Order("C", this.SandwichFactory.CreateSandwich("chicken")));
        expectedCommand.Add(new Order("D", this.SandwichFactory.CreateSandwich("dieppois")));
        expectedCommand.Add(new Order("E", this.SandwichFactory.CreateSandwich("dieppois")));
        expectedCommand.Add(new Order("F", this.SandwichFactory.CreateSandwich("dieppois")));
        

        Command myCommand = this.parser.Parse(
            " A    ham     sandwich    ," +
            "   B   chicken ,   C    chicken   ," +
            "  D dieppois , E      dieppois ,   F    dieppois   ");

        myCommand.GetOrders().Should().BeEquivalentTo(expectedCommand.GetOrders(), options =>
        {
            options.AllowingInfiniteRecursion();
            return options;
        });
    }
    
    [Fact(DisplayName = "a command with more sandwiches than possible index")]
    public void command_with_more_sandwiches_than_possible_indexes()
    {
        ParserConfiguration parserConfiguration = new ParserConfigurationBuilder()
            .WithSandwichSeparator(',')
            .WithIndexSandwichSeparator(' ')
            .WithIndexSandwichChars("ab")
            .Build();
        
        Parser parser = new Parser(parserConfiguration, this.SandwichFactory);
        
        Command expectedCommand = new Command();
        expectedCommand.Add(new Order("a", this.SandwichFactory.CreateSandwich("Dieppois")));
        expectedCommand.Add(new Order("b", this.SandwichFactory.CreateSandwich("Dieppois")));
        expectedCommand.Add(new Order("aa", this.SandwichFactory.CreateSandwich("Dieppois")));
        expectedCommand.Add(new Order("bb", this.SandwichFactory.CreateSandwich("Dieppois")));
        expectedCommand.Add(new Order("aaa", this.SandwichFactory.CreateSandwich("Dieppois")));
        expectedCommand.Add(new Order("bbb", this.SandwichFactory.CreateSandwich("Dieppois")));
        expectedCommand.Add(new Order("aaaa", this.SandwichFactory.CreateSandwich("Dieppois")));
        expectedCommand.Add(new Order("bbbb", this.SandwichFactory.CreateSandwich("Dieppois")));
        expectedCommand.Add(new Order("aaaaa", this.SandwichFactory.CreateSandwich("Dieppois")));
        expectedCommand.Add(new Order("bbbbb", this.SandwichFactory.CreateSandwich("Dieppois")));
        expectedCommand.Add(new Order("aaaaaa", this.SandwichFactory.CreateSandwich("Dieppois")));

        Command myCommand = parser.Parse("a Dieppois, b Dieppois, aa Dieppois, bb Dieppois, aaa Dieppois, bbb Dieppois, aaaa Dieppois, bbbb Dieppois, aaaaa Dieppois, bbbbb Dieppois, aaaaaa Dieppois");
        myCommand.GetOrders().Should().BeEquivalentTo(expectedCommand.GetOrders(), options =>
        {
            options.AllowingInfiniteRecursion();
            return options;
        });
    }
    
    [Fact(DisplayName = "a command with mutliples indexes")]
    public void command_with_mutliples_indexes()
    {
        
        ParserConfiguration parserConfiguration = new ParserConfigurationBuilder()
            .WithSandwichSeparator(',')
            .WithIndexSandwichSeparator(' ')
            .WithIndexSandwichChars("ab")
            .Build();
        
        Parser parser = new Parser(parserConfiguration, this.SandwichFactory);
        Command expectedCommand = new Command();
        expectedCommand.Add(new Order("a", this.SandwichFactory.CreateSandwich("Dieppois")));
        expectedCommand.Add(new Order("b", this.SandwichFactory.CreateSandwich("Dieppois")));
        expectedCommand.Add(new Order("aa", this.SandwichFactory.CreateSandwich("Dieppois")));

        Command myCommand = parser.Parse("a Dieppois, b Dieppois, aa Dieppois");
        myCommand.GetOrders().Should().BeEquivalentTo(expectedCommand.GetOrders(), options =>
        {
            options.AllowingInfiniteRecursion();
            return options;
        });

    }
    
    [Fact(DisplayName = "a command with wrong indexes should return an error")]
    public void command_with_wrong_indexes_should_return_an_error()
    {
        ParserConfiguration parserConfiguration = new ParserConfigurationBuilder()
            .WithSandwichSeparator(',')
            .WithIndexSandwichSeparator(' ')
            .WithIndexSandwichChars("ab")
            .Build();
        
        Parser parser = new Parser(parserConfiguration, this.SandwichFactory);
        Action action = () => parser.Parse("a Dieppois, b Dieppois, bb Dieppois");

        action.Should()
            .Throw<IndexNotValidException>()
            .WithMessage("Error: index 'bb' is not valid.");
        
    }
}
