﻿using FluentAssertions;
using Models;
using SandwichShop.Factory;
using SandwichShop.Utilitaries.Bill;
using System.IO;
using Xunit;

namespace Test;

public class BillFormaterTests
{
    private readonly SandwichFactory sandwichFactory = new JsonSandwichFactory(Directory.GetParent(Directory.GetCurrentDirectory()).Parent.Parent.Parent + @"/Menu.json");
    private readonly BillFormatter _billFormaterLegacy = new BillFormatter(new ClassicMyBillFormatterStrategy());

    [Fact(DisplayName =
        "Command of one ham and one chicken for one client\n" +
        "Should cost 8.5€ for this client")]
    public void BillFormater_1()
    {
        Command command = new Command();
        command.Add(new Order("person_A", this.sandwichFactory.CreateSandwich("ham")));
        command.Add(new Order("person_A", this.sandwichFactory.CreateSandwich("chicken")));

        this._billFormaterLegacy.format(command).Should().Contain("Prix Total: 8,5e");
    }

    [Fact(DisplayName =
        "Command of two ham\n" +
        "Should cost a total of 7€")]
    public void BillFormater_2()
    {
        Command command = new Command();
        command.Add(new Order("person_A", this.sandwichFactory.CreateSandwich("ham")));
        command.Add(new Order("person_B", this.sandwichFactory.CreateSandwich("ham")));

        this._billFormaterLegacy.format(command).Should().Contain("Total: 7e");
    }
}
