using FluentAssertions;
using SandwichShop.CommandSystem;
using SandwichShop.Factory;
using SandwichShop.Utilitaries.Bill;
using System;
using System.IO;
using Xunit;

namespace Test;

public class CommandEngineTests
{
    private readonly SandwichFactory sandwichFactory = new JsonSandwichFactory(Directory.GetParent(Directory.GetCurrentDirectory()).Parent.Parent.Parent + @"/Menu.json");
    private readonly BillFormatter _billFormaterLegacy = new BillFormatter(new ClassicMyBillFormatterStrategy());

    [Fact(DisplayName =
        "Instanciate the engine\n" +
        "With an illegal order separator\n" +
        "Should throw exception")]
    public void CommandEngine_1()
    {
        Action instanciateCommandEngine = () =>
            CommandEngine
                .WithOrderSeparator(' ')
                .WithClientSeparator('+')
                .WithBillFormater(this._billFormaterLegacy)
                .WithSandwichFactory(this.sandwichFactory)
                .Run();

        instanciateCommandEngine.Should()
            .Throw<ArgumentException>()
            .WithMessage("orderSeparator can't be ' '");
    }

    [Fact(DisplayName =
        "Instanciate the engine\n" +
        "With an illegal client separator\n" +
        "Should throw exception")]
    public void CommandEngine_2()
    {
        Action instanciateCommandEngine = () =>
            CommandEngine
                .WithOrderSeparator(',')
                .WithClientSeparator(' ')
                .WithBillFormater(this._billFormaterLegacy)
                .WithSandwichFactory(this.sandwichFactory)
                .Run();

        instanciateCommandEngine.Should()
            .Throw<ArgumentException>()
            .WithMessage("clientSeparator can't be ' '");
    }

    [Fact(DisplayName =
        "Instanciate the engine\n" +
        "With same char as order and client separator\n" +
        "Should throw exception")]
    public void CommandEngine_3()
    {
        Action instanciateCommandEngine = () =>
            CommandEngine
                .WithOrderSeparator(',')
                .WithClientSeparator(',')
                .WithBillFormater(this._billFormaterLegacy)
                .WithSandwichFactory(this.sandwichFactory)
                .Run();

        instanciateCommandEngine.Should()
            .Throw<ArgumentException>()
            .WithMessage("orderSeparator and clientSeparator can't be the same");
    }

    [Fact(DisplayName =
        "Instanciate the engine\n" +
        "With null bill formater\n" +
        "Should throw exception")]
    public void CommandEngine_4()
    {
        Action instanciateCommandEngine = () =>
            CommandEngine
                .WithOrderSeparator(',')
                .WithClientSeparator('+')
                .WithBillFormater(null)
                .WithSandwichFactory(this.sandwichFactory)
                .Run();

        instanciateCommandEngine.Should()
            .Throw<ArgumentNullException>()
            .WithMessage("Value cannot be null. (Parameter 'billFormaterLegacy')");
    }

    [Fact(DisplayName =
        "Instanciate the engine\n" +
        "With null sandwich factory\n" +
        "Should throw exception")]
    public void CommandEngine_5()
    {
        Action instanciateCommandEngine = () =>
            CommandEngine
                .WithOrderSeparator(',')
                .WithClientSeparator('+')
                .WithBillFormater(this._billFormaterLegacy)
                .WithSandwichFactory(null)
                .Run();

        instanciateCommandEngine.Should()
            .Throw<ArgumentNullException>()
            .WithMessage("Value cannot be null. (Parameter 'sandwichFactory')");
    }

    [Fact(DisplayName = 
        "Instanciate the engine\n" +
        "With a legal configuration\n" +
        "Should not throw exception")]
    public void CommandEngine_6()
    {
        Action instanciateCommandEngine = () =>
            CommandEngine
                .WithOrderSeparator(',')
                .WithClientSeparator('+')
                .WithBillFormater(this._billFormaterLegacy)
                .WithSandwichFactory(null);
        
        instanciateCommandEngine.Invoking(engine => engine).Should().NotThrow();
    }
}
