
**Sandwich Shop**

Sandwhich Shop allows it's user to pass multiple sandwich orders and create a bill<br>
Each client has their own personal order, the bill is divided for each client

**Program usage**

The client can interact with the program via the CLI<br>
To make an Order, the client has to fill in their name, followed by the sandwich they wants<br>
If there is multiple clients, they can make a giant Order by separating their Orders<br>
The client can quit the program by entering 'QUIT'

**Models diagram**

[![](https://mermaid.ink/img/pako:eNptUk1PwzAM_StRTiDWP1BxQQyhSYgPTdx6MYnpojVJlbiCavS_kzYpS1l9qBy_Z_u9NCcurERectGA91sFtQNdGRbi3moNRrLbn6JgL06ii_UpjdV9wL-UOERgPkVsZ2qHUqGhykR8WnFmnWJ1jBvmySlTs63ybQP9M2jM0SflqUiUOyGwJZQjx-ckabuPBtmrU-Ky-awmE5bah1xedLembWckfuf1PyfLa1iMy_auzfxvNHl468CQon6l490oWtkz_6xsSRGdR0N2_PqLa4ngI9KU-Kvrxeg5-IZrdBqUDA9l2lBxOmCQzsuQSnDHildmCLyulUD4IBVZx8tPaDxuOHRk970RvCTX4UxKjy2xhl-wgsgH)](https://mermaid.live/edit#pako:eNptUk1PwzAM_StRTiDWP1BxQQyhSYgPTdx6MYnpojVJlbiCavS_kzYpS1l9qBy_Z_u9NCcurERectGA91sFtQNdGRbi3moNRrLbn6JgL06ii_UpjdV9wL-UOERgPkVsZ2qHUqGhykR8WnFmnWJ1jBvmySlTs63ybQP9M2jM0SflqUiUOyGwJZQjx-ckabuPBtmrU-Ky-awmE5bah1xedLembWckfuf1PyfLa1iMy_auzfxvNHl468CQon6l490oWtkz_6xsSRGdR0N2_PqLa4ngI9KU-Kvrxeg5-IZrdBqUDA9l2lBxOmCQzsuQSnDHildmCLyulUD4IBVZx8tPaDxuOHRk970RvCTX4UxKjy2xhl-wgsgH)

**CLI workflow**

[![](https://mermaid.ink/img/pako:eNqFk8tu3DAMRX-FUBfdpD9gFAO007Topq9ZdOMNbdEToTalUFKCQZB_L-UHXI_b1IAN2Dq8vL6UnkzrLZnKRLrPxC19cHgWHGoGvabnK8zJcx4akpqnTwEludYF5AQEGOGWz45pvxjK4jeUWGqvF2NXVk_I9tG1d9Bhm7xc9lwzcu9d30PnZcC0iE3PLz4R-AcSoAp-okuFgqwtX0dwHHKaOOwTfC6v4CIggxe7CJWL3hwOoZo1qY-0wvdZRVs_DGp1Ldj0_V6QIH4NjxZ25VR-zOJ_5hw_YO_-aBXUmzY5ok6oh-PkBMrtuEQyCv7N-ZXOxspRSKMEpsdFcQV77wN81BhJ4csU1dtGDo73OSwGY1fBJ9KxzgMt_GgxlT86C1lHnOK2cOPonbXwtbRSfu-J-CqRZmzIJOU3mnF7iB_2hXN6JxUAoZg16kL_c0wvb6MNV5sfTmcyjkIoaKC1MTdmIN2mzuq5eipFtUl3NFBtSoFF-VWbmp-Vy8Gq91vrdOObqkOd3Y0px-104dZUSTIt0HwwZ-r5N1NCMxw)](https://mermaid.live/edit#pako:eNqFk8tu3DAMRX-FUBfdpD9gFAO007Topq9ZdOMNbdEToTalUFKCQZB_L-UHXI_b1IAN2Dq8vL6UnkzrLZnKRLrPxC19cHgWHGoGvabnK8zJcx4akpqnTwEludYF5AQEGOGWz45pvxjK4jeUWGqvF2NXVk_I9tG1d9Bhm7xc9lwzcu9d30PnZcC0iE3PLz4R-AcSoAp-okuFgqwtX0dwHHKaOOwTfC6v4CIggxe7CJWL3hwOoZo1qY-0wvdZRVs_DGp1Ldj0_V6QIH4NjxZ25VR-zOJ_5hw_YO_-aBXUmzY5ok6oh-PkBMrtuEQyCv7N-ZXOxspRSKMEpsdFcQV77wN81BhJ4csU1dtGDo73OSwGY1fBJ9KxzgMt_GgxlT86C1lHnOK2cOPonbXwtbRSfu-J-CqRZmzIJOU3mnF7iB_2hXN6JxUAoZg16kL_c0wvb6MNV5sfTmcyjkIoaKC1MTdmIN2mzuq5eipFtUl3NFBtSoFF-VWbmp-Vy8Gq91vrdOObqkOd3Y0px-104dZUSTIt0HwwZ-r5N1NCMxw)

**Design patterns**

Fluent :
- Used for the CLI engine
- Prevent the CLI engine to be instanciated without its necessary properties

Strategy :
- Used for the Logger and Bill Formater
- Allows the Logger to print its data in different ways, its Log method adapts to the format parameter
- Allows the Bill Formatter to display the command in different possible ways

Factory :
- Used for the SandwichFactory to create different possible version of itself (JSON or XML)
- Allows the developer to create a SandwichFactory from multiple sources

Builder :
- Used for the ParserConfiguration
- Allows the developer to fill the configuration's properties in a cleaner way
