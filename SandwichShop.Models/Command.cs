using System.Collections.ObjectModel;

namespace Models;

public class Command
{
    private List<Order> orders = new List<Order>();

    public void Add(Order classicOrder)
    {
        this.orders.Add(classicOrder);
    }

    public ReadOnlyCollection<Order> GetOrders() => this.orders.AsReadOnly();
}