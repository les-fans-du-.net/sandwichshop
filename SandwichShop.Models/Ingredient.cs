﻿namespace Models;

public record struct Ingredient
{
    public string Name { get; init; }
    public double Quantity { get; init; }
    public string Unit { get; init; }
}
