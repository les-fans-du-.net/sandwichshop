namespace Models;

public class Order
{
    public string Index;
    public Sandwich Sandwich;
    public Order(string index, Sandwich sandwich)
    {
        Index = index;
        Sandwich = sandwich;
    }
}