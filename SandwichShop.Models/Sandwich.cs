﻿namespace Models;

public record struct Sandwich
{
    public string DisplayName { get; init; }
    public List<string> AcceptedNames { get; init; }
    public double Price { get; init; }
    public List<Ingredient> Ingredients { get; init; }
}
