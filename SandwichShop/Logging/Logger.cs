namespace SandwichShop.Logging;

public class Logger

{
    private readonly string filepath;
    private readonly ILoggingFormat loggingFormat;

    public Logger(ILoggingFormat loggingFormat)
    {
        this.loggingFormat = loggingFormat ?? throw new ArgumentNullException(nameof(loggingFormat));
        this.filepath = Directory.GetParent(Directory.GetCurrentDirectory()).Parent.Parent.Parent + "/Logs.log";
    }

    public string Log(string message, ILoggingFormat optionalLoggingFormat = null)
    {
        string log = optionalLoggingFormat != null ?
            optionalLoggingFormat.Compute(message) :
            this.loggingFormat.Compute(message);

        File.AppendAllTextAsync(this.filepath, log);

        return log;
    }
}