namespace SandwichShop.Logging;

public interface ILoggingFormat
{
    string Compute(string message);
}

public class MessageFormat : ILoggingFormat
{
    public string Compute(string message)
    {
        return $"{message}\n";
    }
}

public class DateAndMessageFormat : ILoggingFormat
{
    public string Compute(string message)
    {
        return $"{DateTime.Now}: {message}\n";
    }
}