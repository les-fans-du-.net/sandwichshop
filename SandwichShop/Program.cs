﻿using SandwichShop.Cli;
using SandwichShop.CommandSystem;
using SandwichShop.Factory;
using SandwichShop.Utilitaries.Bill;

BillFormatter billFormatter = new BillFormatter(new ClassicMyBillFormatterStrategy());

string menuFilepath = Directory.GetParent(Directory.GetCurrentDirectory()).Parent.Parent.Parent + "/Menu.json";

SandwichFactory sandwichFactory = SandwichFactory.Create(SandwichInputFormat.JSON, menuFilepath);

char sandwichSeparator = ',';
char indexSandwichSeparator = ' ';

ParserConfiguration parserConfiguration = new ParserConfigurationBuilder()
    .WithSandwichSeparator(sandwichSeparator)
    .WithIndexSandwichSeparator(indexSandwichSeparator)
    //.WithIndexSandwichChars("ABCDEFGHIJKLMNOPQRSTUVWXYZ")
    .WithIndexSandwichChars("ab")
    .Build();
Parser parser = new Parser(parserConfiguration, sandwichFactory);

CliConfiguration cliConfiguration;
try
{
    cliConfiguration = new CliConfigurationBuilder()
        .WithPreInputMessage("  New order > ")
        .WithExitFlag("quit")
        .WithHelpMessage("\n" +
                         $"  Error during order processing, please follow this pattern :\n" +
                         $"    JohnDoe HamSandwich{sandwichSeparator} SamGamgee HamSandwich\n" +
                         $"    JohnDoe {indexSandwichSeparator} SamGamgee HamSandwich")
        .WithStartMessage("\n" +
                          $"\t\t0─0──0────0 Sandwich Shop ! 0─────0──0─0\n\n" +
                          $"  To make an order, specify your name without space, and the sandwich you dream of\n" +
                          $"    Example: JohnDoe HamSandwich\n\n" +
                          $"  You can also order multiple sandwiches, separated by a '{sandwichSeparator}'\n" +
                          $"    Example: JohnDoe HamSandwich{sandwichSeparator} JohnDoe ChickenVegetable\n\n" +
                          $"  Multiple persons can order on the same command, so bring in your friends !\n" +
                          $"    Example: JohnDoe HamSandwich{sandwichSeparator} SamGamgee HamSandwich\n" +
                          $"         Or: JohnDoe {indexSandwichSeparator} SamGamgee HamSandwich\n\n" +
                          $"\t\t0─0──0────0 Sandwich Shop ! 0─────0──0─0")
        .Build();   
}
catch(Exception e)
{
    Console.WriteLine($"Fatal error: {e.Message}");
    return;
}

try
{
    CliEngine
        .WithParser(parser)
        .WithBillFormater(billFormatter)
        .WithConfiguration(cliConfiguration)
        .Run();
}
catch (Exception e)
{
    Console.WriteLine($"Fatal error: {e.Message}");
}
