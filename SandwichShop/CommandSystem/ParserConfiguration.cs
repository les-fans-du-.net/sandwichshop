namespace SandwichShop.CommandSystem;

public class ParserConfiguration
{
    public char SandwichSeparator;
    public char IndexSandwichSeparator;
    public string IndexSandwichChars;

    public ParserConfiguration(char sandwichSeparator, char indexSandwichSeparator, string indexSandwichChars)
    {
        this.SandwichSeparator = sandwichSeparator;
        this.IndexSandwichSeparator = indexSandwichSeparator;
        this.IndexSandwichChars = indexSandwichChars;
    }
    
}