﻿using Models;
using SandwichShop.Factory;
using SandwichShop.Utilitaries.Bill;

namespace SandwichShop.CommandSystem;

public class CommandEngine :
    CommandEngine_OrderSeparator,
    CommandEngine_ClientSeparator,
    CommandEngine_BillFormater,
    CommandEngine_SandwichFactory
{
    private char orderSeparator;
    private char clientSeparator;
    private BillFormatter _billFormater;
    private SandwichFactory sandwichFactory;

    public static CommandEngine_OrderSeparator WithOrderSeparator(char orderSeparator)
    {
        CommandEngine commandEngine = new CommandEngine();
        commandEngine.orderSeparator = orderSeparator;
        return commandEngine;
    }

    public CommandEngine_ClientSeparator WithClientSeparator(char clientSeparator)
    {
        this.clientSeparator = clientSeparator;
        this.ValidateSeparators();
        return this;
    }

    public CommandEngine_BillFormater WithBillFormater(BillFormatter billFormaterLegacy)
    {
        if (billFormaterLegacy == null)
        {
            throw new ArgumentNullException(nameof(billFormaterLegacy));
        }
        this._billFormater = billFormaterLegacy;
        return this;
    }

    public CommandEngine_SandwichFactory WithSandwichFactory(SandwichFactory sandwichFactory)
    {
        if (sandwichFactory == null)
        {
            throw new ArgumentNullException(nameof(sandwichFactory));
        }
        this.sandwichFactory = sandwichFactory;
        return this;
    }

    public void Run()
    {
        CommandParser commandParser = new CommandParser(
            this.orderSeparator,
            this.clientSeparator,
            this.sandwichFactory);

        Console.WriteLine(
            $"\t\tO─O───O──────O Sandwich Shop ! O──────O───O─O\n\n" +
            $"  To make an order, specify your name without space, and the sandwich you dream of\n" +
            $"  Example: JohnDoe HamSandwich\n\n" +
            $"  You can also order multiple sandwiches, separated by a '{this.orderSeparator}'\n" +
            $"  Example: JohnDoe HamSandwich{this.orderSeparator} JohnDoe ChickenVegetable\n\n" +
            $"  Multiple persons can order on the same command, so bring in your friends !\n" +
            $"  Example: JohnDoe HamSandwich{this.orderSeparator} SamGamgee HamSandwich\n" +
            $"       Or: JohnDoe {this.clientSeparator} SamGamgee HamSandwich\n\n" +
            $"\t\tO─O───O──────O Sandwich Shop ! O──────O───O─O\n");

        while (true)
        {
            Console.Write("  New order > ");
            string commandInput = Console.ReadLine();

            if (commandInput.Trim().StartsWith("quit", StringComparison.OrdinalIgnoreCase))
            {
                break;
            }

            try
            {
                Command command = commandParser.Parse(commandInput);

                string bill = this._billFormater.format(command);

                Console.WriteLine(bill);
            }
            catch (SandwichNotFoundException e)
            {
                Console.WriteLine(e.Message);
            }
            catch (IndexOutOfRangeException)
            {
                Console.WriteLine(
                    $"  The format has to be as below !\n" +
                    $"  Example: JohnDoe HamSandwich{this.orderSeparator} SamGamgee HamSandwich\n" +
                    $"       Or: JohnDoe {this.clientSeparator} SamGamgee HamSandwich\n");
            }
        }
    }

    private void ValidateSeparators()
    {
        if (this.IsIllegalChar(this.orderSeparator))
        {
            throw new ArgumentException($"{nameof(this.orderSeparator)} can't be '{this.orderSeparator}'");
        }
        if (this.IsIllegalChar(this.clientSeparator))
        {
            throw new ArgumentException($"{nameof(this.clientSeparator)} can't be '{this.clientSeparator}'");
        }
        if (this.orderSeparator.Equals(this.clientSeparator))
        {
            throw new ArgumentException($"{nameof(this.orderSeparator)} and {nameof(this.clientSeparator)} can't be the same");
        }
    }

    private bool IsIllegalChar(char character)
    {
        return new char[] { ' ', '\0', '\n', '\t' }.Contains(character);
    }
}
