namespace SandwichShop.CommandSystem;

public class IndexNotValidException: Exception
{
    public IndexNotValidException(string index) : base($"Error: index '{index}' is not valid.")
    {

    }
}