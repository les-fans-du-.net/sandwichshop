using System.Text.RegularExpressions;
using Models;
using SandwichShop.Factory;

namespace SandwichShop.CommandSystem;

public class Parser
{
    private readonly ParserConfiguration _parserConfiguration;
    private readonly SandwichFactory _sandwichFactory;

    public Parser(ParserConfiguration parserConfiguration, SandwichFactory sandwichFactory)
    {
        this._parserConfiguration = parserConfiguration;
        this._sandwichFactory = sandwichFactory;
    }

    public Command Parse(string inputCommand)
    {
        Command myCommand = new Command();
        string[] inputParse = this.FormatOrders(inputCommand);

        int sandwichNumber = 0;
        foreach (string order in inputParse)
        {
            string[] orderSplit = order.Split(this._parserConfiguration.IndexSandwichSeparator);
            if(orderSplit.Length < 2) throw new ArgumentException();
            
            string sandwichInput = formatSandwichInput(orderSplit);
            
            if (!this.validateIndex(orderSplit[0], sandwichNumber))
            {
                Console.WriteLine($"{orderSplit[0]} is not a valid index for {sandwichNumber}");
                throw new IndexNotValidException(orderSplit[0]);
            }
            
            Sandwich sandwich = this.GetSandwich(sandwichInput);
            sandwichNumber++;
            myCommand.Add(new Order(orderSplit[0], sandwich));
        }

        return myCommand;
    }
    
    private string[] FormatOrders(string command)
    {
        return command.Split(this._parserConfiguration.SandwichSeparator)
            .Select(order => Regex.Replace(order, @"\s+", " ").Trim())
            .ToArray();
    }

    private string formatSandwichInput(string[] orderSplit)
    {
        string orderSplitResult = "";
        if (orderSplit.Length > 2)
        {
            orderSplitResult = String.Join(' ', orderSplit, 1, orderSplit.Length - 1);
        }
        else if(orderSplit.Length == 2)
        {
            orderSplitResult = orderSplit[1];
        }

        return orderSplitResult;
    }
    

    private bool validateIndex(string index, int sandwichNumber)
    {
        int numberOfSandwichAppear = sandwichNumber;
        string result = "";
        int occuranceNumber = (sandwichNumber / this._parserConfiguration.IndexSandwichChars.Length) + 1;
        int letterIndex = (sandwichNumber % this._parserConfiguration.IndexSandwichChars.Length);
        
        for (int i = 0; i < occuranceNumber; i++)
        {
            result += this._parserConfiguration.IndexSandwichChars[letterIndex].ToString();
        }
        
        return result == index;
    }
    
    private Sandwich GetSandwich(string sandwichCandidate)
    {
        return this._sandwichFactory.CreateSandwich(sandwichCandidate);
    }

}