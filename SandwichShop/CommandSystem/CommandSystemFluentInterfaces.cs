﻿using SandwichShop.Factory;
using SandwichShop.Utilitaries.Bill;

namespace SandwichShop.CommandSystem;

public interface CommandEngine_OrderSeparator
{
    public CommandEngine_ClientSeparator WithClientSeparator(char clientSeparator);
}

public interface CommandEngine_ClientSeparator
{
    public CommandEngine_BillFormater WithBillFormater(BillFormatter billFormater);
}

public interface CommandEngine_BillFormater
{
    public CommandEngine_SandwichFactory WithSandwichFactory(SandwichFactory sandwichFactory);
}

public interface CommandEngine_SandwichFactory
{
    public void Run();
}