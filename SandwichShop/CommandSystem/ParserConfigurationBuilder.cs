namespace SandwichShop.CommandSystem;

public class ParserConfigurationBuilder
{
    private char _sandwichSeparator;
    private char _indexSandwichSeparator;
    private string _indexSandwichChars;
    
    public ParserConfigurationBuilder WithSandwichSeparator(char sandwichSeparator)
    {
        _sandwichSeparator = sandwichSeparator;
        return this;
    }
    
    public ParserConfigurationBuilder WithIndexSandwichSeparator(char indexSandwichSeparator)
    {
        _indexSandwichSeparator = indexSandwichSeparator;
        return this;
    }
    
    public ParserConfigurationBuilder WithIndexSandwichChars(string indexSandwichChars)
    {
        _indexSandwichChars = indexSandwichChars;
        return this;
    }


    public ParserConfiguration Build()
    {
        if (_sandwichSeparator == '\0'
            || _indexSandwichSeparator == '\0'
            || string.IsNullOrWhiteSpace(_indexSandwichChars))
        {
            throw new ArgumentNullException();
        }
        
        if (this._sandwichSeparator.Equals(this._indexSandwichSeparator))
        {
            throw new ArgumentException($"{nameof(this._sandwichSeparator)} and {nameof(this._indexSandwichSeparator)} can't be the same");
        }

        return new ParserConfiguration(_sandwichSeparator, _indexSandwichSeparator, _indexSandwichChars);
    }


}