﻿using Models;
using SandwichShop.Factory;
using System.Text.RegularExpressions;

namespace SandwichShop.CommandSystem;

public class CommandParser
{
    private readonly char orderSeparator;
    private readonly char clientSeparator;
    private readonly SandwichFactory sandwichFactory;

    public CommandParser(char orderSeparator, char clientSeparator, SandwichFactory sandwichFactory)
    {
        this.orderSeparator = orderSeparator;
        this.clientSeparator = clientSeparator;
        this.sandwichFactory = sandwichFactory;
    }

    public Command Parse(string commandInput)
    {
        Command command = new Command();

        string[] orders = this.FormatOrders(commandInput);

        foreach (string order in orders)
        {
            string[] issuers = this.GetIssuers(order);
            Sandwich sandwich = this.GetSandwich(order);

            foreach (string issuer in issuers)
            {
                command.Add(new Order(issuer, sandwich));
            }
        }

        return command;
    }

    private string[] FormatOrders(string command)
    {
        return command.Split(this.orderSeparator)
            .Select(order => Regex.Replace(order, @"\s+", " ").Trim())
            .ToArray();
    }

    private string[] GetIssuers(string order)
    {
        return order.Split(this.clientSeparator)
            .Select(issuer => issuer.Trim().Split(" ")[0].Trim())
            .ToArray();
    }

    private Sandwich GetSandwich(string order)
    {
        string sandwichName = order.Contains(this.clientSeparator) ?
            order.Split(this.clientSeparator)[order.Split(this.clientSeparator).Length - 1].Trim().Split(" ", 2)[1] :
            order.Split(" ", 2)[1];

        return this.sandwichFactory.CreateSandwich(sandwichName);
    }
}
