namespace SandwichShop.Cli;

public class CliConfigurationBuilder
{
    private string _helpMessage;
    private string _preInputMessage;
    private string _startMessage;
    private string _exitFlag;

    public CliConfigurationBuilder WithStartMessage(string startMessage)
    {
        _startMessage = startMessage;
        return this;
    }

    public CliConfigurationBuilder WithPreInputMessage(string preInputMessage)
    {
        _preInputMessage = preInputMessage;
        return this;
    }

    public CliConfigurationBuilder WithHelpMessage(string helpMessage)
    {
        _helpMessage = helpMessage;
        return this;
    }

    public CliConfigurationBuilder WithExitFlag(string exitFlag)
    {
        _exitFlag = exitFlag;
        return this;
    }

    public CliConfiguration Build()
    {
        if (string.IsNullOrEmpty(_startMessage)
            || string.IsNullOrEmpty(_preInputMessage)
            || string.IsNullOrEmpty(_helpMessage)
            || string.IsNullOrEmpty(_exitFlag))
        {
            throw new ArgumentNullException();
        }

        return new CliConfiguration()
        {
            HelpMessage = _helpMessage,
            PreInputMessage = _preInputMessage,
            StartMessage = _startMessage,
            ExitFlag = _exitFlag
        };
    }
}