namespace SandwichShop.Cli;

public class CliConfiguration
{
    public string HelpMessage { get; set; }
    public string PreInputMessage { get; set; }
    public string StartMessage { get; set; }
    public string ExitFlag { get; set; }
}