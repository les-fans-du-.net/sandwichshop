using Models;
using SandwichShop.CommandSystem;
using SandwichShop.Factory;
using SandwichShop.Logging;
using SandwichShop.Utilitaries.Bill;

namespace SandwichShop.Cli;

public class CliEngine : CliEngine_WithParser, CliEngine_WithBillFormater, CliEngine_WithConfiguration
{
    private Parser _parser;
    private CliConfiguration _cliConfiguration;
    private BillFormatter _billFormater;
    private Logger _logger;

    private CliEngine()
    {
        this._logger = new Logger(new DateAndMessageFormat());
    }

    public static CliEngine_WithParser WithParser(Parser parser)
    {
        CliEngine cliEngine = new CliEngine();
        cliEngine._parser = parser;
        return cliEngine;
    }

    public CliEngine_WithBillFormater WithBillFormater(BillFormatter billFormatter)
    {
        this._billFormater = billFormatter;
        return this;
    }

    public CliEngine_WithConfiguration WithConfiguration(CliConfiguration cliConfiguration)
    {
        this._cliConfiguration = cliConfiguration;
        return this;
    }

    public void Run()
    {
        Console.WriteLine(this._cliConfiguration.StartMessage);

        while (true)
        {
            Console.Write(this._cliConfiguration.PreInputMessage);
            string commandInput = Console.ReadLine();
            this._logger.Log(commandInput);

            if (commandInput.Trim().StartsWith(this._cliConfiguration.ExitFlag, StringComparison.OrdinalIgnoreCase))
            {
                break;
            }

            try
            {
                Command myCommand = this._parser.Parse(commandInput);

                string bill = this._billFormater.format(myCommand);

                Console.WriteLine(bill);
            }
            catch (SandwichNotFoundException e)
            {
                Console.WriteLine(e.Message);
            }
            catch (Exception)
            {
                Console.WriteLine("Wrong order format");
            }
        }
    }
}