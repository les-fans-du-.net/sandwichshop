using SandwichShop.Utilitaries.Bill;

namespace SandwichShop.Cli;

public interface CliEngine_WithParser
{
    public CliEngine_WithBillFormater WithBillFormater(BillFormatter billFormatter);
}

public interface CliEngine_WithBillFormater
{
    public CliEngine_WithConfiguration WithConfiguration(CliConfiguration cliConfiguration);
}

public interface CliEngine_WithConfiguration
{
    public void Run();
}