using Models;

namespace SandwichShop.Utilitaries.Bill;

public class BillFormatter
{
    private MyBillFormatterStrategy _myBillFormatterStrategy;

    public BillFormatter(MyBillFormatterStrategy myBillFormatterStrategy)
    {
        _myBillFormatterStrategy = myBillFormatterStrategy;
    }

    public string format(Command myCommand)
    {
        return this._myBillFormatterStrategy.format(myCommand);
    }
}
