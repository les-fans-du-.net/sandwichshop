using Models;

namespace SandwichShop.Utilitaries.Bill;

public interface MyBillFormatterStrategy
{
    public string format(Command myCommand);
}