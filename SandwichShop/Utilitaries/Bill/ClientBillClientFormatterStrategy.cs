using System.Text;
using Models;

namespace SandwichShop.Utilitaries.Bill;

public class ClientMyBillClientFormatterStrategy : MyBillFormatterStrategy
{
    public string format(Command myCommand)
    {
        Dictionary<string, double> totalPerClient = new Dictionary<string, double>();
        StringBuilder result = new StringBuilder("\nYour bill:\n");

        Dictionary<string, Dictionary<Sandwich, int>> clientsSandwiches = this.getSandwichDictionnaryByClient(myCommand);

        foreach (KeyValuePair<string, Dictionary<Sandwich, int>> clientSandwiches in clientsSandwiches)
        {
            result.Append($"  Client: {clientSandwiches.Key}\n");

            totalPerClient.Add(clientSandwiches.Key, 0);

            foreach (KeyValuePair<Sandwich, int> sandwich in clientSandwiches.Value)
            {
                result.Append($"    {sandwich.Key.DisplayName} x {sandwich.Value} ({sandwich.Key.Price}e/u)\n");

                totalPerClient[clientSandwiches.Key] += sandwich.Key.Price * sandwich.Value;

                this.AddIngredients(sandwich.Key, sandwich.Value, ref result);
            }
        }

        this.AddPrices(totalPerClient, ref result);

        return result.ToString();
    }

    private Dictionary<string, Dictionary<Sandwich, int>> getSandwichDictionnaryByClient(Command myCommand)
    {
        Dictionary<string, Dictionary<Sandwich, int>> clientSandwiches = new Dictionary<string, Dictionary<Sandwich, int>>();

        foreach (var classicOrder in myCommand.GetOrders())
        {
            Dictionary<Sandwich, int> sandwiches = clientSandwiches[classicOrder.Index];

            if (sandwiches == null)
            {
                Dictionary<Sandwich, int> newSandwich = new Dictionary<Sandwich, int>();
                newSandwich.Add(classicOrder.Sandwich, 1);

                clientSandwiches.Add(classicOrder.Index, newSandwich);
            }
            else
            {
                if (sandwiches.ContainsKey(classicOrder.Sandwich))
                {
                    sandwiches[classicOrder.Sandwich] += 1;
                }
                else
                {
                    sandwiches.Add(classicOrder.Sandwich, 1);
                }
            }

        }

        return clientSandwiches;
    }

    private void AddIngredients(Sandwich sandwich, int sandwichCount, ref StringBuilder result)
    {
        foreach (Ingredient ingredient in sandwich.Ingredients)
        {
            result.Append($"\t{ingredient.Name}: {ingredient.Quantity * sandwichCount}{ingredient.Unit}\n");
        }
    }

    private void AddPrices(Dictionary<string, double> totalPerClient, ref StringBuilder result)
    {
        double totalPrice = 0;

        foreach (KeyValuePair<string, double> client in totalPerClient)
        {
            result.Append($"Total for {client.Key}: {client.Value}e\n");
            totalPrice += client.Value;
        }

        result.Append($"Total: {totalPrice}e\n");
    }
}