using System.Text;
using Models;

namespace SandwichShop.Utilitaries.Bill;

public class ClassicMyBillFormatterStrategy : MyBillFormatterStrategy
{
    public string format(Command myCommand)
    {
        StringBuilder result = new StringBuilder("");

        myCommand.GetOrders().ToList().ForEach(order =>
        {
            result.Append($"{order.Index} {order.Sandwich.DisplayName}\n");

            this.AddIngredients(order.Sandwich, ref result);
        });

        this.AddPrices(myCommand.GetOrders().ToList(), ref result);

        return result.ToString();
    }

    private void AddIngredients(Sandwich sandwich, ref StringBuilder result)
    {
        foreach (Ingredient ingredient in sandwich.Ingredients)
        {
            result.Append($"\t{ingredient.Name}: {ingredient.Quantity}{ingredient.Unit}\n");
        }
    }

    private void AddPrices(List<Order> orders, ref StringBuilder result)
    {
        double totalPrice = 0;

        foreach (Order order in orders)
        {
            totalPrice += order.Sandwich.Price;
        }

        result.Append($"Prix Total: {totalPrice}e");
    }
}