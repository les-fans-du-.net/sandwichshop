using System.ComponentModel;
using Models;

namespace SandwichShop.Factory;

public abstract class SandwichFactory
{
    public abstract Sandwich CreateSandwich(string sandwichName);
    
    public static SandwichFactory Create(SandwichInputFormat sandwichInputFormat, string filePath)
    {
        if (sandwichInputFormat == SandwichInputFormat.JSON)
            return new JsonSandwichFactory(filePath);
        else if (sandwichInputFormat == SandwichInputFormat.XML)
            throw new NotImplementedException($"the format {sandwichInputFormat.ToString()} is not supported");
        
        
        throw new NotSupportedException($"the format {sandwichInputFormat.ToString()} is not supported");
    }

}

public enum SandwichInputFormat
{
    [Description("JSON")]
    JSON,
    [Description("XML")]
    XML
}
   