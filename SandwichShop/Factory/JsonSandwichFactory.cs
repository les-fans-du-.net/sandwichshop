﻿using Fastenshtein;
using Models;
using System.Text.Json;

namespace SandwichShop.Factory;

public class JsonSandwichFactory: SandwichFactory
{
    private readonly List<Sandwich> menu;

    public JsonSandwichFactory(string menuFilepath)
    {
        this.menu = this.GetMenu(menuFilepath);
    }

    private List<Sandwich> GetMenu(string menuFilepath)
    {
        return JsonSerializer.Deserialize<List<Sandwich>>(File.ReadAllText(menuFilepath), new JsonSerializerOptions()
        {
            PropertyNameCaseInsensitive = true,
        });
    }
    
    public override Sandwich CreateSandwich(string sandwichName)
    {
        List<SandwichNameScore> sandwiches = new List<SandwichNameScore>();

        this.menu.ForEach(sandwich =>
        {
            sandwich.AcceptedNames.ForEach(name =>
            {
                int score = Levenshtein.Distance(name.ToLower(), sandwichName.ToLower());
                //Console.WriteLine($"{name} - {sandwichName} : {score}");
                if (score <= 5)
                {
                    sandwiches.Add(new SandwichNameScore()
                    {
                        Sandwich = sandwich,
                        Score = score,
                    });
                    return;
                }
            });
        });

        if (sandwiches.Count == 0)
        {
            throw new SandwichNotFoundException(sandwichName);
        }

        sandwiches.Sort();
        return sandwiches.First().Sandwich;
    }

    private class SandwichNameScore : IComparable
    {
        public Sandwich Sandwich { get; set; }
        public double Score { get; set; }

        public int CompareTo(object other)
        {
            if (other == null)
            {
                return 1;
            }

            SandwichNameScore otherSandwich = other as SandwichNameScore;

            return otherSandwich != null ? this.Score.CompareTo(otherSandwich.Score) : 1;
        }
    }
}
