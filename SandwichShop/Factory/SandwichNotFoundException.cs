﻿namespace SandwichShop.Factory;

public class SandwichNotFoundException : Exception
{
    public SandwichNotFoundException(string sandwichName) : base($"Error: sandwich '{sandwichName}' does not exist.")
    {

    }
}
